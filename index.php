<?php
/* Домашнее задание к лекции 4.2 «Запросы SELECT, INSERT, UPDATE и DELETE»

Базовое задание:
Необходимо создать TODO-приложение (список дел). Поскольку создание таблиц в полной мере мы еще не проходили,
предлагаю использовать дамп данных (https://netology-university.bitbucket.io/php/homework/4.2-query/dump.txt) и создать с его помощью таблицу tasks.

В приложении необходимо реализовать возможность:
    - выводить список дел;
    - добавлять новые дела;
    - отмечать дела как выполненные;
    - удалять дела.

Расширенное задание:
Добавить возможность редактировать текст запланированного дела.
Задание для самых отчаянных:

Добавить возможность сортировки списка дел по:
    - описанию;
    - дате добавления;
    - статусу.

Пример того, как все должно работать в «максимальной комплектации» (http://university.netology.ru/u/vfilipov/homework2.php)

Кстати, данный пример опубликован со всеми правами на редактирование для всех, т.е. любой студент может добавлять и удалять все данные из этого приложения. Надеюсь на вашу порядочность :)

Рекомендую делать по порядку, от простого к сложному.
*/

session_start();

error_reporting(E_ALL);
ini_set('display_errors', 1);

function getParamGet($name)
{
    return array_key_exists($name, $_GET) ? $_GET[$name] : null;
}

function getParamPost($name)
{
    return array_key_exists($name, $_POST) ? $_POST[$name] : null;
}

function getParamSession($name)
{
    return array_key_exists($name, $_SESSION) ? $_SESSION[$name] : null;
}

$opt = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES => false,
];
//$pdo = new PDO("mysql:host=localhost;charset=utf8;dbname=global", "lmoiseev", "neto1490");
$pdo = new PDO("mysql:host=localhost;port=3306;charset=utf8;dbname=global", "root", "root", $opt);

$action = getParamGet('action');
$id = (int)getParamGet('id');
$description = '';

if ($action == 'delete') {
    $stmt = $pdo->prepare("DELETE FROM `tasks` WHERE `id` = :id");
    $stmt->execute(['id' => $id]);
} elseif ($action == 'edit') {
    $stmt = $pdo->prepare("SELECT * FROM `tasks` WHERE `id` = :id");
    $stmt->execute(['id' => $id]);
    $row = $stmt->fetch();

    $description = $row['description'];
    $_SESSION['taskId'] = $row['id'];
} elseif ($action == 'done') {
    $stmt = $pdo->prepare("UPDATE `tasks` SET `is_done` = TRUE WHERE `id` = :id");
    $stmt->execute(['id' => $id]);
}

$order = 'date_added';
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (array_key_exists('save', $_POST) && !empty(getParamPost('description'))) {
        if (!empty($id) && getParamPost('save') == 'Сохранить') {
            $stmt = $pdo->prepare("UPDATE `tasks` SET `description` = :desc WHERE `id` = :id");
            $stmt->execute(['id' => $id, 'desc' => getParamPost('description')]);
            unset($_SESSION['taskId']);
            header('Location: index.php');
        } elseif (getParamPost('save') == 'Добавить') {
            $stmt = $pdo->prepare("INSERT INTO `tasks` (`description`, `date_added`) VALUES (:desc, NOW())");
            $stmt->execute(['desc' => getParamPost('description')]);
        }
    } elseif (array_key_exists('sort', $_POST)) {
        $order = getParamPost('sort_by');
    }
}

?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>PHP-19. Task 4.2</title>
    <style>
        table {
            border-spacing: 0;
            border-collapse: collapse;
            margin-top: 10px;
        }

        table td, table th {
            border: 1px solid #ccc;
            padding: 5px;
        }

        table th {
            background: #eee;
        }
    </style>
</head>
<body>
<h1>Список дел на сегодня</h1>
<div style="float: left">
    <form method="POST">
        <input type="text" name="description" placeholder="Описание задачи" value="<?= $description ?>"/>
        <input type="submit" name="save" value="<?= getParamSession('taskId') == null ? 'Добавить' : 'Сохранить' ?>"/>
    </form>
</div>
<div style="float: left; margin-left: 20px;">
    <form method="POST">
        <label for="sort">Сортировать по:</label>
        <select name="sort_by">
            <option <?= $order == 'date_added' ? 'selected ' : '' ?>value="date_added">Дате добавления</option>
            <option <?= $order == 'is_done' ? 'selected ' : '' ?>value="is_done">Статусу</option>
            <option <?= $order == 'description' ? 'selected ' : '' ?>value="description">Описанию</option>
        </select>
        <input type="submit" name="sort" value="Отсортировать"/>
    </form>
</div>
<div style="clear: both"></div>

<table>
    <tr>
        <th>Описание задачи</th>
        <th>Дата добавления</th>
        <th>Статус</th>
        <th></th>
    </tr>
    <?php

    // Не получилось добиться сортировки при использовании подготовленного выражения. Не смог разобраться почему закомментированный текст не работает.
    //$stmt = $pdo->prepare("SELECT * FROM `tasks` ORDER BY :order");
    //$stmt->execute(['order' => $order]);
    //$result = $stmt;

    $result = $pdo->query("SELECT * FROM `tasks` ORDER BY `{$order}`");
    foreach ($result as $row) {
        ?>
        <tr>
            <td><?= $row['description'] ?></td>
            <td><?= $row['date_added'] ?></td>
            <td><?= $row['is_done'] ? "<span style='color: green;'>Выполнено</span>" : "<span style='color: orange;'>В процессе</span>" ?></td>
            <td>
                <a href="<?= "?id={$row['id']}&action=edit" ?>">Изменить</a>
                <a href="<?= "?id={$row['id']}&action=done" ?>">Выполнить</a>
                <a href="<?= "?id={$row['id']}&action=delete" ?>">Удалить</a>
            </td>
        </tr>
        <?php
    }
    ?>
</table>
</body>
</html>
